import React, { Component } from "react";
import Movies from "./Movies";
import MovieTypes from "./MovieTypes";
import { Row, Alert, ListGroup } from "reactstrap";
import { movieRetriever } from "../helper/helper";

export default class MovieList extends Component {
  state = {
    isLoaded: false,
    currentPage: 1,
    count: 0,
    movies: [],
    searchParams: {
      page: 1,
      title: "Avengers",
      type: ""
    }
  };

  handlePageItemClick = pageNumber => {
    this.setState({
      currentPage: pageNumber
    });
  };

  getMovies = () => {
    const movieInfo = {
      movieUpdate: this.handleMovieUpdate,
      movieData: this.state
    };
    movieRetriever(movieInfo);
  };

  handleMovieTypeChange = e => {
    let movieType = e.target.value;
    this.setState(
      prevState => ({
        searchParams: { ...prevState.searchParams, type: movieType }
      }),
      this.getMovies
    );
  };

  handleMovieUpdate = movies => {
    if (movies.Error === "Movie not found!") {
      this.setState({
        isLoaded: false,
        pageCount: 10,
        count: 0,
        movies: []
      });
    }
    this.setState({
      isLoaded: true,
      pageCount: 10,
      count: Object.keys(movies.Search).length,
      movies: movies
    });
  };

  render() {
    const movieData = this.state;
    return (
      <div>
        <Alert color="primary">
          List of movies from online site {process.env.REACT_APP_MOVIE_DB}
          <br />
          Showing {movieData.count} of {movieData.movies.totalResults}.
        </Alert>
        <Row>
          <div className="col-2">
            <ListGroup>
              <MovieTypes
                handleMovieTypeChange={this.handleMovieTypeChange}
                selectedMovieType={movieData.searchParams.type}
              />
            </ListGroup>
          </div>
          <div className="col-10">
            <Movies
              movieData={movieData}
              movieUpdate={this.handleMovieUpdate}
              handlePageItemClick={this.handlePageItemClick}
            />
          </div>
        </Row>
      </div>
    );
  }
}

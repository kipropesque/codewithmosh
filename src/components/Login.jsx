import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Form, Label, Input, FormGroup, Button, FormFeedback } from "reactstrap";
import Joi from "joi-browser";

export default class Login extends Component {
  state = {
    credentials: { email: "", password: "" },
    errors: { email: null, password: null}
  };
  
  handleSubmit = e => {
    e.preventDefault();
    this.validate();
  };

  handleChange = e => {
    const { name, value } = e.currentTarget;
    this.setState(
      prevState => ({
        credentials: { ...prevState.credentials, [name]: value }
      }),
      this.validate
    );
  };

  schema = {
    email: Joi.string().required(),
    password: Joi.string().required()
  };

  validate = () => {
    const result = Joi.validate(this.state.credentials, this.schema, {
      abortEarly: false
    });
    
    let errors = this.state.errors;
    if (result.error === null) {
        this.setState({errors: {email: null, password:null}})
        return null;
    } 

    let errorDetails = result.error.details;

    errorDetails.map(error => {
      errors = { ...errors, [error["path"]]: error.message };
      return errors;
    });
    this.setState({errors: errors})
  };

  render() {
      const { errors } = this.state;
    return (
      <div>
        <Form className="px-4 py-3" onSubmit={this.handleSubmit}>
          <FormGroup>
            <Label for="exampleDropdownFormEmail1">Email address</Label>
            <Input
              type="email"
              className="form-control"
              id="exampleDropdownFormEmail1"
              name="email"
              placeholder="email@example.com"
              onChange={this.handleChange}
              invalid={errors.email}
              valid={!!errors.email ? false : true}
            />
            <FormFeedback valid>Oh noes! that name is already taken</FormFeedback>
            <FormFeedback>Sweet! that name is available</FormFeedback>
          </FormGroup>
          
          <FormGroup>
            <Label for="exampleDropdownFormPassword1">Password</Label>
            <Input
              type="password"
              className="form-control"
              id="exampleDropdownFormPassword1"
              name="password"
              placeholder="Password"
              onChange={this.handleChange}
            />
          </FormGroup>
          <div className="form-check">
            <Input
              type="checkbox"
              className="form-check-input"
              id="dropdownCheck"
            />
            <Label className="form-check-label" for="dropdownCheck">
              Remember me
            </Label>
          </div>
          <Button type="submit" color="primary">
            Sign in
          </Button>
        </Form>
        <div className="dropdown-divider"></div>
        <Link className="dropdown-item" to="#">
          New around here? Sign up
        </Link>
        <Link className="dropdown-item" to="#">
          Forgot password?
        </Link>
      </div>
    );
  }
}

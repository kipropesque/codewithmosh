import React, { Component } from "react";
import { Link } from "react-router-dom";
export default class Movie extends Component {

  render() {
    const movie = this.props.movie;
    return (
      <tr>
        <td><Link to={`/movies/${movie.imdbID}`}>{movie.Title}</Link></td>
        <td>{movie.Type}</td>
        <td>{movie.Year}</td>
        <td>{movie.imdbID}</td>
        <td></td>
      </tr>
    );
  }
}

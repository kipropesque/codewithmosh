import React from 'react'

const SingleMovie = ( { match } ) => {
    return (
        <div>
            Movie {match.params.imdbID}
        </div>
    )
}

export default SingleMovie;
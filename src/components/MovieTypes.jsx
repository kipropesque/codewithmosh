import React, { Component } from "react";
import { ListGroupItem } from "reactstrap";

class MovieTypes extends Component {
  render() {
    const { handleMovieTypeChange, selectedMovieType } = this.props;
    const types = ["Movie", "Series", "Episode"];
    return types.map((movieType, i) => (
      <ListGroupItem
        active={selectedMovieType === movieType}
        value={movieType}
        key={i}
        tag="button"
        onClick={handleMovieTypeChange}
      >
        {movieType}
      </ListGroupItem>
    ));
  }
}

export default MovieTypes;

import React, { Component } from "react";
import { Table } from "reactstrap";
import Movie from "./Movie";
import Pagination from "./pagination/Pagination";
import { movieRetriever }  from '../helper/helper';
import PropTypes from 'prop-types';

class Movies extends Component {
  componentDidMount() {
    try {
      movieRetriever(this.props);
    } catch (e) {
      console.log("something went wrong");
    }
  }

  render() {
    const {
      movieData,
      movieData: {
        movies: { Search: movieList }
      },
      handlePageItemClick
    } = this.props;

    if (movieData.isLoaded) {
      return (
        <React.Fragment>
          <Table dark>
            <thead>
              <tr>
                <th>Title</th>
                <th>Genre</th>
                <th>Stock</th>
                <th>Rate</th>
                <th>#</th>
              </tr>
            </thead>
            <tbody>
              {Object.keys(movieList).map(key => (
                <Movie key={key} movie={movieList[key]} />
              ))}
            </tbody>
          </Table>
          <Pagination
            movieData={movieData}
            handlePageItemClick={handlePageItemClick}
          />
        </React.Fragment>
      );
    } else {
      return <p>Loading...</p>;
    }
  }
}

Pagination.propTypes = {
  movieData: PropTypes.object,
  handlePageItemClick: PropTypes.func
}

export default Movies;

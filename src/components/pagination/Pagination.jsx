import React, { Component } from "react";
import PageItem from "./PageItem";
import _ from "lodash";

export default class Pagination extends Component {
  render() {
    const { movieData, handlePageItemClick } = this.props;
    //using lodash to create an array from a number so as to use map, since foreach does not work
    let pageCount = movieData.movies.totalResults / movieData.pageCount;
    pageCount = pageCount > 10 ? 10 : pageCount;
    let pages = _.range(1, pageCount);

    return (
      <nav aria-label="Page navigation">
        <ul className="pagination">
          <li className="page-item">
            <a className="page-link" href="#/">
              Previous
            </a>
          </li>
          {pages.map((page, i) => (
            <PageItem
              key={i}
              pageNumber={page}
              pageItemClick={handlePageItemClick}
              currentPage={movieData.currentPage}
            />
          ))}
          <li className="page-item">
            <a className="page-link" href="#/">
              Next
            </a>
          </li>
        </ul>
      </nav>
    );
  }
}

import React, { Component } from "react";

export default class PageItem extends Component {
  render() {
    const { pageNumber, pageItemClick, currentPage } = this.props;
    let activeClass =
      currentPage === pageNumber ? "page-item active" : "page-item";

    return (
      <li className={activeClass}>
        <a
          className="page-link"
          href="#/"
          onClick={() => pageItemClick(pageNumber)}
        >
          {pageNumber}
        </a>
      </li>
    );
  }
}

import React from "react";
import { Nav } from "reactstrap";
import { NavLink } from "react-router-dom";

const NavBar = () => {
  return (
    <Nav className="navbar navbar-expand-lg navbar-light bg-light">
      <NavLink className="navbar-brand" to="/">
        Home
      </NavLink>
      <NavLink className="navbar-brand" to="/customers">
        Customers
      </NavLink>
      <NavLink className="navbar-brand" to="/rentals">
        Rentals
      </NavLink>
      <NavLink className="navbar-brand" to="/login">
        Login
      </NavLink>
    </Nav>
  );
};

export default NavBar;

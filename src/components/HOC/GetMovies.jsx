import React, { Component } from "react";

const withGetMovies = WrappedComponent => {
  class WithGetMovies extends Component {
    getMovies = () => {
      const {
        movieUpdate,
        movieData: {
          searchParams: { title, type }
        }
      } = this.props;
      const url = process.env.REACT_APP_MOVIE_DB;
      const apiKey = process.env.REACT_APP_MOVIE_DB_API_KEY;

      const movieDBUrl = url.concat("/?apiKey=").concat(apiKey);

      //We are now getting movies via HOC arguments
      const batManMovies = movieDBUrl
        .concat("&s=")
        .concat(title)
        .concat("&type=")
        .concat(type);

      //Rewrite using await
      fetch(batManMovies)
        .then(response => {
          return response.json();
        })
        .then(movies => {
          movieUpdate(movies);
        })
        .catch(error => {
          console.log("Uh oh, we have errors:", error);
        });
    };

    render() {
      return <WrappedComponent getMovies={this.getMovies} {...this.props} {...this.state} />;
    }
  }
  return WithGetMovies;
};

export default withGetMovies;

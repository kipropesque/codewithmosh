// Movie retriever function for getting
function movieRetriever(movies) {
  const {
    movieUpdate,
    movieData: {
      searchParams: { title, type }
    }
  } = movies;

  const url = process.env.REACT_APP_MOVIE_DB;
  const apiKey = process.env.REACT_APP_MOVIE_DB_API_KEY;

  const movieDBUrl = url.concat("/?apiKey=").concat(apiKey);

  //We are now getting movies via HOC arguments
  const moviesUrl = movieDBUrl
    .concat("&s=")
    .concat(title)
    .concat("&type=")
    .concat(type);

  //Rewrite using await
  fetch(moviesUrl)
    .then(response => {
      return response.json();
    })
    .then(movies => {
      movieUpdate(movies);
    })
    .catch(error => {
      console.log("Uh oh, we have errors:", error);
    });

  return movies;
}

export default movieRetriever;

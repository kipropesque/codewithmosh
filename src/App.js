import React, { Component } from "react";
import "./App.css";
import { Container } from "reactstrap";
import { Route, Switch, Redirect } from "react-router-dom";
import Customers from "./components/Customers";
import Rentals from "./components/Rentals";
import NotFound from "./components/NotFound";
import MovieList from "./components/MovieList";
import NavBar from "./components/NavBar";
import SingleMovie from "./components/SingleMovie";
import Login from "./components/Login";

class App extends Component {
  render() {
    return (
      <Container className="App">
        <NavBar />
        <Switch>
          <Route path="/movies/:imdbID" component={SingleMovie} />
          <Route path="/customers" component={Customers} />
          <Route path="/rentals" component={Rentals} />
          <Route path="/not-found" component={NotFound} />
          <Route path="/movies" component={MovieList} />
          <Route path="/login" component={Login} />
          <Redirect exact from="/" to="/movies" />
          <Redirect to="/not-found" />
        </Switch>
      </Container>
    );
  }
}
export default App;
